package noobbot;

import java.util.Arrays;

import data.objects.TipoPista; 

public class TrackPiece {
	private int id;
	private TipoPista type;
	private TrackPiece next;
	private TrackPiece previous;
	private boolean linhaDeChegada = false;
	private double posLinhaDeChegada = 0;
	private double length = 0;
	private boolean passou = false;
	
	private double angle = 0;
	private double radius = 0;
	public int[] indexComposition;
	public double velocidadeNecessaria;
	public double melhoria = 1.0;
	public int numberOfSwitches = 0;
	
	boolean retaDoTurbo = false;
	
	public TrackPiece() {
		this.length = 0;
		this.angle = 0;
		this.radius = 0;
	}

	public boolean isPassou() {
		return passou;
	}

	public void setPassou(boolean passou) {
		this.passou = passou;
	}

	public TipoPista getType() {
		return type;
	}

	public void setType(TipoPista type) {
		this.type = type;
	}

	public int getNumberOfSwitches() {
		return numberOfSwitches;
	}

	public void setNumberOfSwitches(int numberOfSwitches) {
		this.numberOfSwitches = numberOfSwitches;
	}

	public TrackPiece getNext() {
		return next;
	}

	public void setNext(TrackPiece next) {
		this.next = next;
	}

	public TrackPiece getPrevious() {
		return previous;
	}

	public void setPrevious(TrackPiece previous) {
		this.previous = previous;
	}

	public boolean isLinhaDeChegada() {
		return linhaDeChegada;
	}

	public void setLinhaDeChegada(boolean linhaDeChegada) {
		this.linhaDeChegada = linhaDeChegada;
	}

	public boolean isRetaDoTurbo() {
		return retaDoTurbo;
	}

	public void setRetaDoTurbo(boolean retaDoTurbo) {
		this.retaDoTurbo = retaDoTurbo;
	}

	public double getPosLinhaDeChegada() {
		
		
		
		return posLinhaDeChegada;
	}

	public void setPosLinhaDeChegada(double posLinhaDeChegada) {
		this.posLinhaDeChegada = posLinhaDeChegada;
	}

	public double getLength() {
		return length;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getAngle() {
		return angle;
	}

	public double getVelocidadeNecessaria() {
		return velocidadeNecessaria;
	}

	public void setVelocidadeNecessaria(double velocidadeNecessaria) {
		this.velocidadeNecessaria = velocidadeNecessaria;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public int[] getIndexComposition() {
		return indexComposition;
	}

	public void setIndexComposition(int[] indexComposition) {
		this.indexComposition = indexComposition;
	}

	public double getMelhoria() {
		return melhoria;
	}

	public void setMelhoria(double melhoria) {
		this.melhoria += melhoria;
	} 
	
	public double getVelBoa(double radiusDifference){
		if(this.getAngle() > 0){
			return ( Math.pow((this.getRadius()-radiusDifference)/3.5, 0.55))*this.melhoria;
		}
		else return ( Math.pow((this.getRadius()+radiusDifference)/3.5, 0.55))*this.melhoria;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrackPiece other = (TrackPiece) obj;
		if (id != other.id)
			return false;
		return true;
	}


	
	
}

