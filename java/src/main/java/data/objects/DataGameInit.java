package data.objects;

import com.google.gson.annotations.Expose;

public class DataGameInit {

	@Expose
	private Race race;

	public Race getRace() {
		return race;
	}

	public void setRace(Race race) {
		this.race = race;
	}

}